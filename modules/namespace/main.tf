
module "labels" {
  source     = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-tags.git//modules/namespace"
  supporters = var.metadata.supporters
  solution   = var.metadata.solution
}

resource "kubernetes_namespace" "this" {
  metadata {
    name   = var.name
    labels = merge(var.labels, module.labels.tags)
  }
  timeouts {
    delete = var.timeout_delete
  }
}
