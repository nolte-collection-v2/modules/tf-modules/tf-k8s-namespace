variable "name" {
  type = string
}
variable "metadata" {
  type = object({
    supporters = string
    solution   = string
  })
}
variable "timeout_delete" {
  default = "5m"
}

variable "labels" {
  default     = {}
  description = "Additional labels (e.g. `map('BusinessUnit','XYZ')`"
}
